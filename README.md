# Testing simple distributed system components

## Failure detector

A gossip style failure detector. Key point of this distributed algorithm includes:

- Each new node joining the system will have a random subset of membership list of its introducer.
- The new node then starts to broadcast its existence (heartbeat) to a random subset of membership list at each round in local lamport timestamp.
- Each node receiving a heartbeat updates its local copy of membership list. (The updated value must have a larger lamport timestamp).
- Each heartbeat sent is a random subset of sender's membership list.
- When a nodes discover any member on its membership list exceeding some timeout before updating, it marks it as failed and stops sending the failed nodes in its own heartbeat.
- After another timeout, failed nodes are removed from membership list.

## Tech highlight

An [RPC](src/sds_component/sds_rpc.py) module is built using decorator. Ideas are

- The client part will use pickle to serialize the function name, all arguments and send the pickled data over socket to server.
- The server will unpickle the data, and call the corresponding function based on the decoded function name.

## TODO:

Some components that will be implemented in the future:

- A simple distributed file system.
- A fault-tolerant graph processing framework based on the existing components.
