default: deploy run

deploy:
	sh setup.sh
	pwd > /tmp/rootdir.txt

run:
	python src/sds_component/cs425sdsshell.py

clean:
	rm /tmp/rootdir.txt
	sh clear.sh

.PHONY: clean deploy run