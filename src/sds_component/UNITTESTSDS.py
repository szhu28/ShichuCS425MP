import time
import inspect

import unittest
import sds_util
from sds_rpc import SDSRPCClient, SDSRPCServer, rpc_client_class_member

logger = sds_util.colored_log()


class TestSdsRpcModule(unittest.TestCase):
    def test_rpc_call(self):
        class UserRPC(SDSRPCClient):
            """Client side base class"""

            @rpc_client_class_member
            def myfun(self, a=1, b=2):
                return locals()

        class ServerRPC(SDSRPCServer):
            def rpc_myfun(self, a=3, b=4):
                return (a, b)

        #server:
        server = ServerRPC()
        addr = server.start(timeout_period=0.1)
        logger.info("RPC function set: " + str(server.rpc_set))
        #client:
        client = UserRPC(addr)
        assert client.myfun() == (1, 2)
        assert client.myfun(4, 6) == (4, 6)
        assert client.myfun(b=12, a=-1) == (-1, 12)
        server.stop()


if __name__ == '__main__':
    unittest.main()
