"""Utilities facilitating other modules."""

import time
import logging
import socket
import argparse
import subprocess
import pickle
import signal
import os
import sys

MAX_WORKERS = 8
NUM_WORKERS = MAX_WORKERS
BUFSIZE = 4096

with open("/tmp/rootdir.txt", "rt") as f:
    rootdir = f.readline()

ENV_CONFIG = {
    'SSH_PORT': "49222",
    'SSH_HOST': "localhost",
    'VM_ROOT': rootdir+"/tests/vm/vm",
    # For dept. desktop
    # 'PYTHON3': "/home/shichu/usr/anaconda3/bin/python3",
    # For home laptop
    # 'PYTHON3': "/home/shichu/anaconda3/bin/python3",
    'PROTOC': "/home/shichu/usr/anaconda3/bin/protoc",
    'COLD_START': True
}

if 'shichu-Inspiron-5566' in socket.gethostname():
    ENV_CONFIG['PYTHON3'] = "/home/shichu/anaconda3/bin/python3"
else:
    ENV_CONFIG['PYTHON3'] = "python3"


def get_config(wid):
    """For internal use only, returns a copy of the config based on the VM ID"""
    config = ENV_CONFIG.copy()
    # -1 -> 1 standby, 0 -> 2 master, 1-8 -> 3 -> 10 workers
    vm_num = wid + 2
    config['VM_ROOT'] += str(vm_num)
    return config


SRC = rootdir+"/src"


class Timer:
    """Class for counting the elapse time in second."""

    def __init__(self):
        """Initialize the self.start with a reset"""
        self.start = 0
        self.reset()

    def reset(self):
        """Reset the timer and start timing."""
        self.start = time.time()

    def read(self):
        """return the elapse time since last reset call"""
        return time.time() - self.start


# ------------------------------------------------------ Reference BEGIN
# LOGGING_FORMAT = "%(asctime)s;%(process)d;%(levelname)-8s;%(message)s"
# logging.basicConfig(level=logging.DEBUG, format=LOGGING_FORMAT)
logging.basicConfig(level=logging.DEBUG)


def colored_log(worker_id=None, enable=True):
    """Returns a colored Logger object for the current module

    Colorful logging configuration modified based on
    https://stackoverflow.com/a/384125/5426033

    Question: wasn't able to use the __main__ scope WORKER_ID other than
    explicit passing through the parameter
    """
    _black, red, green, yellow, blue, _magenta, _cyan, white = range(8)
    # The background is set with 40 plus the number of the color, and the foreground with 30
    # These are the sequences need to get colored ouput
    reset_seq = "\033[0m"
    color_seq = "\033[1;%dm"
    bold_seq = "\033[1m"

    def formatter_message(message, use_color=enable):
        """Format the format string"""
        if use_color:
            message = message.replace("$RESET", reset_seq).replace(
                "$BOLD", bold_seq)
        else:
            message = message.replace("$RESET", "").replace("$BOLD", "")
        return message

    colors = {
        'DEBUG': green,
        'INFO': white,
        'WARNING': blue,
        'ERROR': yellow,
        'CRITICAL': red
    }

    class ColoredHandler(logging.StreamHandler):
        """Subclass a Handler required for a log object"""

        if worker_id is None:
            strwid = ""
        else:
            strwid = "{:>2d}--".format(worker_id)
        FORMAT = "%(asctime)s;%(process)5d;%(module)-16s;%(levelname)-19s;" + \
            strwid + ";%(message)s"
        COLOR_FORMAT = formatter_message(FORMAT, True)

        def __init__(self):
            logging.StreamHandler.__init__(self)
            self.setFormatter(
                ColoredFormatter(self.COLOR_FORMAT, use_color=enable))

    class ColoredFormatter(logging.Formatter):
        """Subclass a Formatter required for a handler class"""

        def __init__(self, msg, use_color=enable):
            logging.Formatter.__init__(self, msg)
            self.use_color = use_color

        def format(self, record):
            levelname = record.levelname
            if self.use_color and levelname in colors:
                levelname_color = color_seq % (
                    30 + colors[levelname]) + levelname + reset_seq
                record.levelname = levelname_color
            # print(bytes(logging.Formatter.format(self, record).encode('utf8')))
            return logging.Formatter.format(self, record)

    log = logging.getLogger(__name__)
    log.handlers = [ColoredHandler()]
    log.propagate = False
    return log


logger = colored_log()

# ------------------------------------------------------ Reference END


def parse_manager(strarg=None):
    """Parse command line arguments

    For user to set up the distributed system works

    Returns: args containing
        workertype {int} -- Assigned worker type -1:standby 0:master 1:worker
        host {str} -- IP address of the aggregator
        port {int} -- port number
        codestart {bool} -- Deploy and .py files to VMs
        log {bool} -- Output log to file on each VM
    """

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-l",
        "--log",
        action="store_true",
        help="output log to file on each VM")
    parser.add_argument("workertype", type=int, default=1)
    # Boolean parser, inspired by https://stackoverflow.com/a/15008806/5426033
    codestart_parser = parser.add_mutually_exclusive_group(required=False)
    codestart_parser.add_argument(
        "--codestart",
        help="Deploy the .py files to VMs",
        dest='codestart',
        action='store_true')
    codestart_parser.add_argument(
        "--no-codestart",
        help="Do not deploy the .py files to VMs",
        dest='codestart',
        action='store_false')
    parser.set_defaults(codestart=True)

    parser.add_argument("-H", "--host", help="IP address", type=str)
    parser.add_argument("-p", "--port", help="port number", type=int)

    if strarg:
        args = parser.parse_args(strarg)
    else:
        args = parser.parse_args()
    return args


def remote_spawn_caller(config=get_config(0),
                        node_type=1,
                        src_root=SRC,
                        debug_mode=True):
    """ssh to destination and spawn process there

    Returns the RPC address of established remote call. Raise an [TODO]
    exception if fail remotely.
    """

    cold_start = config['COLD_START']
    ssh_host = config['SSH_HOST']
    ssh_port = config['SSH_PORT']
    python3 = config['PYTHON3']
    vm_root = config['VM_ROOT']

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # TODO this might cause issue since it's local host
    sock.bind((socket.gethostname(), 0))
    sock.listen()
    addr = sock.getsockname()
    # logger.warning("Listening at " + str(addr) +
    #                " for response of spawning node")
    if cold_start:
        # logger.warning("Default to coldstart!")
        cmd = f"scp -qP {ssh_port} {src_root}/sds_component/*.py {ssh_host}:{vm_root}/src/sds_component/"
        # logger.info(cmd)
        subprocess.run(cmd, shell=True)
        # logger.info("source files copied to " + str(ssh_host))

    cmd = f"ssh {ssh_host} -p {ssh_port} 'cd {vm_root} ;" + {
        False: " nohup ",
        True: ""
    }[debug_mode] + f" {python3} src/sds_component/sds_daemon.py {node_type} -H \"" + addr[0] + "\" -p " + str(
        addr[1]) + " " + {
            True: "--codestart",
            False: "--no-codestart"
    }[cold_start] + {
            False: " > noh.txt 2>&1 & ",
            True: ""
    }[debug_mode] + " '"
    # logger.info(cmd)
    if debug_mode:
        proc = subprocess.Popen(cmd, shell=True)
        # logger.debug("PID: " + str(proc.pid) + ": " + cmd)
    else:
        subprocess.run(cmd, shell=True)

    # logger.info("Daemon constructed on " + str(ssh_host))

    # accept connection and receive the rpc_server addr
    # TODO delete, for debug purpose only
    sock.settimeout(5)
    with sock.accept()[0] as feedback:
        mesg = bytes()
        while True:
            tmp = feedback.recv(BUFSIZE)
            if tmp == bytes():
                break
            mesg += tmp
        if mesg == bytes():
            raise Exception("remote_spawn failed")
        ret_addr = pickle.loads(mesg)
    sock.close()
    return ret_addr


def remote_spawn_callee(args):
    """directly called in __main__ from the node that is spawning

    Returns the overarching daemon that was created. The daemon will have the
    rpc_server field assigned to a online TCP RPC server.
    """

    import master
    import worker
    node_type = args.workertype
    fail_flag = False
    if node_type == 1:  # worker node
        daemon = worker.WorkerDaemon()
        daemon.rpc_server = worker.MasterRPCServer()
    elif node_type == 0:  # master node
        daemon = master.MasterDaemon()
        daemon.rpc_server = master.SDSShellRPCServer()
    elif node_type == -1:  # standby node
        daemon = master.MasterDaemon(standby=True)
        daemon.rpc_server = master.SDSShellRPCServer()
    else:
        logger.critical("Starting daemon with unknown type of " +
                        str(node_type))
        fail_flag = True
    # Start the RPC server
    if not fail_flag:
        ret_addr = daemon.rpc_server.start(1)
        # allow RPC module to execute daemon level methods
        daemon.rpc_server.daemon = daemon
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((args.host, args.port))
        if not fail_flag:
            sock.sendall(pickle.dumps(ret_addr))
    return daemon
