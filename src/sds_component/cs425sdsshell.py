import argparse
import subprocess
import socket
import sys
import pickle
import time
import signal
# sys.path.append("sds_component")

import sds_util
import failure_detector
import sds_rpc
from sds_rpc import rpc_client_class_member

SRC = sds_util.SRC
# Same configuration across workers
NUM_WORKERS = sds_util.NUM_WORKERS
MAX_WORKERS = sds_util.MAX_WORKERS

# Create module (script) level logger object
logger = sds_util.colored_log()


class SDSShellRPCClient(sds_rpc.SDSRPCClient):
    """SDSShell client class"""

    @rpc_client_class_member
    def start(self, standby, num_workers, debug_mode, log_enable):
        """instruct the master to spawn the worker / standby"""
        return locals()

    @rpc_client_class_member
    def stop(self):
        """Stop the remote server"""
        return locals()

    @rpc_client_class_member
    def shell(self, cmd, nodelist=None, block=True):
        """Execute the same shell command on all workers

        Arguments: cmd {str} -- shell command

        Keyword Arguments: nodelist {list of int} -- only executed on the list
            of nodes defined by logic id number (default: {None}, all workers
            nodes). Note 0 and -1 are reserved for master and second_master
            nodes, respectively.
        """
        return locals()

    @rpc_client_class_member
    def ml_join(self, wid):
        """A new node joining the membership list"""
        return locals()

    @rpc_client_class_member
    def ml_leave(self, wid):
        """Stop a node and pop it from membership list"""
        return locals()
        # This switches may be impl by master

    @rpc_client_class_member
    def dfs_put(self, localfile, sdffile):
        """Write to file"""
        return locals()

    @rpc_client_class_member
    def dfs_get(self, sdffile, localfile):
        """Read from file"""
        return locals()

    @rpc_client_class_member
    def dfs_ls(self, sdffile):
        """List all VM addresses where this file is currently being stored"""
        return locals()

    @rpc_client_class_member
    def dfs_store(self, wid):
        """List all files currently stored at the machine"""
        return locals()

    @rpc_client_class_member
    def dfs_delete(self, sdffile):
        """Delete file"""
        return locals()


class SDSShell(SDSShellRPCClient):
    """A class describing how users can use and interact with the distributed
    system.

    This class includes a object which is a subclass of the grpc class dealing
    with remote call to master server on the distributed system. It also
    includes a same type object towards the standby master. SDSS should only
    know the ssh (address, port) of the designated (fixed) master node.
    Everything else of the SDS should be hided from SDSShell users. It does keep
    a list of logical worker IDs.

    The purpose of most class methods is to process arguments and form
    appropriate argument for corresponding grpc calls

    Any failing of remote call will result a auto-switch to master2 and
    designating new master2 node randomly
    """

    def __init__(self,
                 num_workers=NUM_WORKERS,
                 standby=True,
                 debug=False,
                 log_enable=False):
        """Initialize the parameters

        Keyword Arguments: num_workers {int} -- number of workers (default:
            {NUM_WORKERS}) second_master {bool} -- Designate a standby master
            backup (default: {True}) log_enable {bool} -- enable logging to file
            in VMs (default: {True}) TODO need to resolve name conflict of
            logging file if multiple instances exists.
        """
        self.num_workers = num_workers
        self.standby = standby
        self.log_enable = log_enable
        self.nodes = []  # list of worker id
        self.master = 0
        # This is defined in SDSRPCClient class and its name can't be changed.
        self.rpc_server_addr = None
        self.debug_mode = debug

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def start(self):
        """start the server and sds functionality"""

        # Spawn the RPC server at master
        self.rpc_server_addr = sds_util.remote_spawn_caller(
            config=sds_util.get_config(self.master),
            node_type=0,
            debug_mode=self.debug_mode)
        # instruct the master to spawn the worker / standby
        super().start(self.standby, self.num_workers, self.debug_mode,
                      self.log_enable)

    def pshell(self, cmd, nodelist=None, block=True):
        """run shell cmd and print the output nicely

        It calls the shell function with the same arguments. Only print results
        in blocking mode.
        """

        ret = self.shell(cmd, nodelist, block)
        if not block:
            return
        for key, out in ret.items():
            print(f"---------@worker {key}:")
            print(out.decode("utf8"))

    def ml_crash(self, wid):
        """Emulate the node crashes by sending SIGKILL to it"""

        # TODO Use sys.excepthook to deal with master fail
        # https://docs.python.org/3.6/library/sys.html#sys.excepthook
        if wid == 1:
            pass
        elif wid == -1:
            pass
        else:
            pass

    def graph_pregel(self, module_class):
        """Run a user-specified graph algorithm based on the Pregel API

        User must define his/her function for the Pregel API by subclassing the
        {TODO} class. The function will then deploy the .py file to workers
        before executing.

        Arguments: module_class {[str]} -- ["module name" + "." + "subclass
            name"]
        """
        pass


if __name__ == "__main__":
    # Using with clause to ensure the remote process is killed upon exitting
    # with SDSShell(num_workers=2, standby=False) as DAEMON:
    # with SDSShell(standby=False, debug=True) as DAEMON:
    with SDSShell(standby=False, debug=True) as DAEMON:
        DAEMON.start()
        time.sleep(10)
        DAEMON.ml_leave(5)
        time.sleep(10)
        # for i in range(5,7):
        #     DAEMON.ml_join(i)
        #     time.sleep(6)
        # print(i)
        # DAEMON.shell("pwd", nodelist=[2, 4, 6])
        # print(DAEMON.pshell("pwd"))

    # command to kill all remaining processes by pattern:
    # ps -ef | grep spawn.py | grep -v grep | awk '{print $2}' | xargs kill -9
    # test for grep "apollo/apollo-14/movies"
