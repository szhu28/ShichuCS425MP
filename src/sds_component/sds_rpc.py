"""A naive RPC implementation using decorator that completely ignores security.

Usage: In the client subclass, decorate the proposed RPC functions with
rpc_client_class_member. Argument shall not include self, and the body of the
function must be return locals(). In server subclass, implement the function
with a prefix 'rpc_'. The server side function can have more argument, but the
shared argument is determined by the client side.

TODO add more functionality such as blocking, timeout or even streaming.
"""

import pickle
import socket
import threading

import sds_util

MAX_CONNECTION = sds_util.MAX_WORKERS  # For socket.listen
TIMEOUT = 2  # Default period of RPC server accept to check stop sign
BUFSIZE = sds_util.BUFSIZE

logger = sds_util.colored_log()


def rpc_client_class_member(fun):
    """Decorate the function to run it as RPC over server"""

    # Register fun.__name__ to the RPC namelist.

    def decorated_fun(self, *args, **kw):
        """define the behavior of decorated function

        Both positional argument and keyword arguments are support! Since
        expanding an empty tuple doesn't cause any shift *tupe(). Only picklable
        objects are supported as arguments. Split out the self argument since
        this is a class member decorator
        """
        # https://docs.python.org/3/library/pickle.html#what-can-be-pickled-and-unpickled
        # Requires local call returns locals()
        fun_namespace = fun(self, *args, **kw)
        del fun_namespace['self']
        pickled_bytes = pickle.dumps([fun.__name__, fun_namespace])
        # Since I'm using the class data member, this line alone makes me feel
        # better to put the entire decorator inside the SDSRPCify class!
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # TODO delete, for debug purpose only
            # sock.settimeout(5)

            sock.connect(self.rpc_server_addr)
            sock.sendall(pickled_bytes)
            sock.shutdown(socket.SHUT_WR)
            # RPC executing remotely
            returned_bytes = bytes()
            while True:
                tmp = sock.recv(4096)
                if tmp != bytes():
                    returned_bytes += tmp
                else:
                    break

        return pickle.loads(returned_bytes)

    return decorated_fun


class SDSRPCClient:
    """RPC client side base class.

    Any subclass needs to add @rpc_client_class_member decorator to register a
    function as RPC, and use the fixed data member "self.rpc_server_addr" to
    establish the connection"""

    # The decorator is actually executed while the class is being defined, HMMM
    def __init__(self, addr=None):
        """Initialize the server address"""
        self.rpc_server_addr = addr


class SDSRPCServer:
    """Server side base class"""

    def __init__(self, host=socket.gethostname()):
        # get all methods from an instance:
        # https://stackoverflow.com/a/39061905/5426033
        self.rpc_set = set([
            func for func in dir(self)
            if callable(getattr(self, func)) and func.startswith("rpc_")
        ])
        self.host = host
        self.rpc_server_addr = None
        self.__sigterm = False

    def __rpc_execute(self, bytecodes):
        """Run the server side function"""

        fun_name, kwargs = pickle.loads(bytecodes)
        fun_name = 'rpc_' + fun_name
        # logger.debug("New task executing: " + str(fun_name) + "(**" +
                    #  str(kwargs) + ")")
        if fun_name in self.rpc_set:
            tmp = getattr(self, fun_name)(**kwargs)
            # logger.debug("New task returning:" + str(tmp))
            return tmp
        else:
            raise Exception("method " + fun_name +
                            " not implemented on server.")

    def __task_threading(self, task_sock):
        # logger.debug("New task received from " + str(task_sock.getsockname()))
        received_bytes = bytes()
        while True:
            tmp = task_sock.recv(BUFSIZE)
            if tmp != bytes():
                received_bytes += tmp
            else:
                break
        task_sock.shutdown(socket.SHUT_RD)
        task_sock.sendall(pickle.dumps(self.__rpc_execute(received_bytes)))
        task_sock.shutdown(socket.SHUT_WR)

    def __start_rpc_service(self, sock):
        """Start rpc service. call the __rpc_execute upon receiving message"""
        while True:
            if self.__sigterm:
                sock.close()
                break
            try:
                # logger.debug("rpc_server accept tick")
                with sock.accept()[0] as task_sock:
                    # For simplicity, everything is linear here. No threads allowed
                    self.__task_threading(task_sock)
            except socket.timeout:
                pass

    def start(self, timeout_period=TIMEOUT):
        """Start the server with a new thread."""

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(timeout_period)
        sock.bind((self.host, 0))
        sock.listen(MAX_CONNECTION)
        self.rpc_server_addr = sock.getsockname()
        # logger.info("RPC server at " + str(sock.getsockname()))
        thread = threading.Thread(
            target=self.__start_rpc_service, kwargs={'sock': sock})
        thread.start()
        return self.rpc_server_addr

    def stop(self):
        """Stop the server"""
        self.__sigterm = True
