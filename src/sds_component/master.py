"""Defines behavior of a master that is responsible for talking with users

Masters serve as the role to receive instruction from users and distribute tasks
to workers.

It acts as a server to utilize sds_rpc module to communicate with user.
At the same time, it also acts as a client to utilize the grpc to communicate
with workers.

Optionally and by default there is a stand-by master that should keep sync with
the main master. (TODO serialize and copy with a 'diff' sense?)

Master specification should be statically stored or (TODO loded from a
configuration file. e.g. available worker IP pools)

Reason not to directly subclass grpc class but instead add its instance as a
data member is that grpc doesn't support multiple arguments and the fact that
python doesn't support function overloading by name. Therefore use a class
intance to create an extra abstraction layer to accommodate more dynamic user
input.
"""

import random
import math

import sds_daemon
import sds_rpc
import signal
import sds_util
from sds_rpc import rpc_client_class_member

logger = sds_util.colored_log()

MAX_WORKERS = sds_util.MAX_WORKERS


class SDSShellRPCServer(sds_rpc.SDSRPCServer):
    """RPC server run on master for user shell client functions"""

    def __init__(self):
        # This is the correct way to call chained constructor in python
        super().__init__()
        self.daemon = None

    def rpc_start(self, standby, num_workers, debug_mode, log_enable):
        """Start up the distributed system, setup membership list and start FD"""
        global logger
        logger = sds_util.colored_log(self.daemon.wid)

        self.daemon.debug_mode = debug_mode
        # Create own TCP data port and FD ports
        self.daemon.addr_table = self.daemon.setup_ports()
        # Start master's FD
        self.daemon.start_fdtor()
        # Join each worker one by one.
        if num_workers > MAX_WORKERS:
            logger.error("Trimming to max workers allowed: " +
                         str(MAX_WORKERS))
        for wid in range(1, num_workers + 1):
            self.rpc_ml_join(wid)

    def rpc_stop(self):
        """Stop the entire service."""
        return self.daemon.stop()

    def rpc_shell(self, cmd, nodelist=None, block=True):
        """Parse commands and run corresponding cmd on desired nodes

        invoke same cmd on nodes in nodelist
        """

        output = {}
        if not nodelist or self.daemon.wid in nodelist:
            output[self.daemon.wid] = self.daemon.sds_exec(cmd, block)

        if not nodelist:
            nodelist = list(self.daemon.workers.keys())
        for wid in nodelist:
            if wid in self.daemon.workers:
                rpc = self.daemon.workers[wid]
                output[wid] = rpc.shell(cmd, block)
        return output

    def rpc_ml_join(self, wid):
        """A new node joining the membership list"""
        if wid == -1:
            # TODO create second standby master
            pass
        else:
            self.daemon.join_worker(wid)

    def rpc_ml_leave(self, wid):
        """A new node joining the membership list"""
        if wid == -1:
            # TODO change second standby master
            pass
        elif wid == 0:
            # TODO switch to secondary master and spawn new 2nd master
            pass
        else:
            self.daemon.leave_worker(wid)


class MasterRPCClient(sds_rpc.SDSRPCClient):
    """Client on master node to invoke RPC on worker nodes"""

    @rpc_client_class_member
    def start(self, param):
        """start the workers given the RPC server is ready"""
        return locals()

    @rpc_client_class_member
    def stop(self):
        """Stop the worker"""
        return locals()

    @rpc_client_class_member
    def shell(self, cmd, block=True):
        """Invoke shell command on a worker"""
        return locals()


class MasterDaemon(sds_daemon.SDSDaemon):
    def __init__(self, standby=False):
        super().__init__()
        self.wid = 0  # Master id set to 0
        self.standby = standby
        self.rpc_server = None
        self.workers = {}  # dict of MasterRPCClient class

    def stop(self):
        """stop daemon TODO Shutdown all working nodes and standby shutdown
        master itself

        Implement the SDSDaemon stop virtual function. This function shouldn't
        be called directly unless the connection to client dies and will be
        called by the 'with' clause to cleanup. A recommended way to stop is
        from client stop call which invoke rpc_stop function and this function.
        """
        success = True
        for wid in list(self.workers.keys()):
            success = success and self.leave_worker(wid)

        self.rpc_server.stop()  # close the RPC server
        # stop the FD at last since it's no network communication
        super().stop()  # close the FD
        return success

    def join_worker(self, wid):
        """A new worker joins the SDS

        Spawn the new process on the worker based on configuration.
        pass current addr_table etc. to it and start its FD
        """
        self.workers[wid] = MasterRPCClient(
            sds_util.remote_spawn_caller(
                config=sds_util.get_config(wid),
                node_type=1,
                debug_mode=self.debug_mode))
        param = {
            kw: getattr(self, kw)
            for kw in ['master_id', 'standby', 'standby_id', 'log_enable']
        }
        param['wid'] = wid
        keys = list(self.addr_table.keys())

        # Trim to only allow max of sqrt(nodes) membership list to start a node
        # for better scaling.
        max_copy_addr = max(4, math.sqrt(len(keys)))
        if len(keys) > max_copy_addr:
            randkeys = random.sample(keys, max_copy_addr)
            param['addr_table'] = {
                key: self.addr_table[key]
                for key in randkeys
            }
        else:
            param['addr_table'] = self.addr_table

        # logger.critical(f"param.addr_table:{param['addr_table']}")
        new_entry = self.workers[wid].start(param)
        self.fdtor.node_join(new_entry)  # This would block!

    def leave_worker(self, wid):
        """Worker wid voluntarily leaves the system

        Shutdown worker wid normally.

        Arguments:
            wid {int} -- worker id
        """
        rpc_server = self.workers[wid]
        try:
            res = False
            res = rpc_server.stop()
        except ConnectionRefusedError:
            logger.warning(f"Connection to worker {wid} lost, can't stop it.")
        if not res:
            return False
        else:
            del self.workers[wid]
            return True
