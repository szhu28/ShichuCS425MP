def return_locals_rpc_decorator(fun):
    def decorated_fun(*args, **kw):
        local_args = fun(*args, **kw)
        print(local_args)
        fun_parameters = fun.__annotations__
        final_parameters = {
            a: list(args)[int(b[-1]) - 1]
            for a, b in fun_parameters.items() if a != 'return'
        }
        # [print((a,b)) for a,b in fun_parameters.items()]
        return final_parameters

    return decorated_fun


# @return_locals_rpc_decorator
# def my_funct(a, b, c):
#     return a + b + c


@return_locals_rpc_decorator
def my_funct(a: "val1", b: "val2", c: "val3") -> int:
    return a + b + c


print(my_funct(10, 20, 30))