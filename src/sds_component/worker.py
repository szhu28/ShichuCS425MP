"""
Daemon running on each worker node.

Each running worker task will set a flag of failure_tolerant to decide if it can be
interrupt and restart upon a failure detector returns spot a failure.
e.g. grep is failure_tolerant while graph operation is not.
When a failure is detected and the current task is not failure_tolerant, a global
flag will be set and checked by the worker regularly to stop.
"""

import sys
import socket
import threading
import signal
import argparse
import time
import logging
import pickle

import sds_util
import sds_rpc
import failure_detector
import sds_daemon

logger = sds_util.colored_log()


class MasterRPCServer(sds_rpc.SDSRPCServer):
    def __init__(self):
        super().__init__()
        self.daemon = None

    # @rpc_client_class_member
    def rpc_start(self, param):
        """start the workers given the RPC server is ready"""
        daemon = self.daemon
        daemon.wid = param['wid']
        global logger
        logger = sds_util.colored_log(daemon.wid)
        ret = daemon.setup_ports()

        daemon.master_id = param['master_id']
        daemon.standby = param['standby']
        daemon.standby_id = param['standby_id']
        daemon.log_enable = param['log_enable']
        daemon.addr_table = param['addr_table']
        # logger.critical(f"daemon.addr_table:{daemon.addr_table}")

        daemon.start_fdtor(daemon.addr_table)
        return ret

    # @rpc_client_class_member
    def rpc_stop(self):
        """Stop the worker

        Invoked by the master, shutdown RPC server and then FD and return True
        for sucess
        """
        super().stop()  # stop the RPC server
        super(type(self.daemon), self.daemon).stop()  # stop the FD
        return True

    # @rpc_client_class_member
    def rpc_shell(self, cmd, block=True):
        return self.daemon.sds_exec(cmd, block)


class WorkerDaemon(sds_daemon.SDSDaemon):
    """Main daemon process running on every worker"""

    def __init__(self):
        """Initialize the parameters"""

        super().__init__()
        self.task_list = []
        self.addr_table = None
        self.fdtor = None  # failure_detector.FailureDetector()
        self.udp_hb = None
        self.udp = None
        self.tcp = None
