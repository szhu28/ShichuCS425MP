"""Failure detector class and its relating exception.

Use pickle to serialize UDP messages. TODO: It's said to be slow. Consider
switching to Google Proto Buffer, especially for graph transmission.
"""
# Standard module import
import pickle
import socket
import random
import math
import threading

# User module import
import sds_util

DEFAULT_TIMEOUT = 0.5  # default timeout value
BUF = 4096
# empirical: 5 will fail, 6 rarely, 7 looks safe
ROUND_FAIL = 4
ROUND_CLEAN = 2*ROUND_FAIL

# class FailureDetectorException(Exception):
#     """Exception to be thrown when a failure is detected."""
#     pass

logger = sds_util.colored_log()


class FailureDetector:
    """
    Failure detector (FD) of the daemon

    It runs upon a separate UDP socket to send heartbeat to current membership
    list. Sending heartbeat is done in blocking call, assuming sending
    congestion is trivial. Receiving is also done in blocking mode, with a
    timeout counting down to required detecting duration.

    A FD raises an exception when a failure is spotted. The main daemon is then
    responsible to decide what to do with the exception.

    Attributes: cid: The id of the worker mlist:
        {id:[id_timestamp,id_local_hb_num]} addr_table: refer to same variable
        in daemon. Responsible for updating it by removing the failed entry.
        type is {cid:(udp_hb_addr, tcp_addr)}
        hb_num: local hearthbeat number
        sock: reference to udp_hb in the daemon, for send/recv heartbeat
        timeout: Round time for heartbeat check
        sigterm: a reference to daemon's sigterm, type: [bool term, bool fail, [failed_id int]]
            term: sigterm from daemon
            fail: failed node detected, output to daemon which resets it
            failed_id int: A list of current failing nodes id's
    """

    def __init__(self,
                 sock,
                 wid,
                 sigterm,
                 addr_table=None,
                 timeout=DEFAULT_TIMEOUT):
        """Initialize the class instance

        The member_table is passed by reference and will be modified by the FD
        once a failure happens.
        """
        self.addr_table = addr_table
        self.timeout = timeout
        self.wid = wid
        global logger
        logger = sds_util.colored_log(self.wid)
        self.hb_num = 0
        self.mlist = {self.wid: [self.hb_num, self.hb_num]}
        if addr_table is not None:
            self.mlist.update({cid: [0, self.hb_num] for cid in addr_table})
        self.sock = sock
        self.sigterm = sigterm
        assert len(self.sigterm) == 1, "fdtor.sigterm initialized len >1"
        self.sigterm += [False, []]
        self.lock = threading.Lock()

        self.__query_set = set()

    def start_service(self):
        """Start the FD service. Intended to be called in the daemon"""

        timer = sds_util.Timer()
        # Snooping sigterm[0] from daemon
        while not self.sigterm[0]:
            timer.reset()
            with self.lock:
                self.gossip_out()
            # logger.debug("Gossipping finishes in " + str(timer.read()) + "s.")
            self.collect_heartbeat(timer)
        logger.warning("FD exits")

    def collect_heartbeat(self, timer):
        """Collects all the incoming heartbeat and merges with the membership list
        """
        while True:
            remaining_time = self.timeout - timer.read()
            if remaining_time > 0.001:
                self.sock.settimeout(remaining_time)
                try:
                    msg, addr = self.sock.recvfrom(BUF)
                    msg = pickle.loads(msg)
                    if msg[0] == 'HB':
                        self.merge(msg[1:], addr)
                    else:  # communication other than regular heartbeat
                        assert len(msg) == 2, "PUT/GET message more than 2 ele"
                        self.cmd_dispatch(msg[0], msg[1], addr)
                except socket.timeout:
                    pass
            else:
                break

    def cmd_dispatch(self, event, args, addr):
        """Resolve commands from the incoming message

        call the corresponding procedure based on event and args

        Arguments:
            event {str} -- command type
            args {list} -- arguments
            addr {addr} -- message source address
        """

        if event == "GET":
            nodesinfo = {
                arg: [self.addr_table[arg], self.mlist[arg][0]]
                for arg in args if arg in self.addr_table
            }
            self.sock.sendto(pickle.dumps(['PUT', nodesinfo]), addr)

        elif event == "PUT":
            with self.lock:
                for wid, info in args.items():
                    if wid not in self.__query_set:
                        continue
                    self.addr_table.update({wid: info[0]})
                    self.mlist[wid] = [info[1], self.hb_num]
                    logger.info("Worker " + str(self.wid) +
                                " discovered worker " + str(wid) + " !")
                    self.__query_set.remove(wid)

    def node_join(self, addr_table_entry):
        """New node join by the master daemon. This would block!"""
        wid = next(iter(addr_table_entry))
        with self.lock:
            if wid not in self.mlist:
                self.addr_table.update(addr_table_entry)
                self.mlist[wid] = [0, self.hb_num]

    def gossip_out(self):
        """Form the membership message and send it through sock

        local time is dropped from the mlist first. Then serialize and send.
        """
        self.hb_num += 1  # don't forget to increase local heart beat
        self.mlist[self.wid][0] = self.hb_num
        self.mlist[self.wid][1] = self.hb_num

        msg_list = [(key, val[0]) for key, val in self.mlist.items()
                    if key not in self.sigterm[2]]
        msg_list.insert(0, 'HB')
        mlist_msg = pickle.dumps(msg_list)
        tmp = random.sample(*self.get_avail_ids())
        logger.debug(
            f"FD tick round {self.hb_num}, mlist:{set(self.mlist.keys())}, failed:{self.sigterm[2]}, gos to {tmp}, query:{self.__query_set}"
        )
        for cid in tmp:
            self.sock.sendto(mlist_msg, self.addr_table[cid][0])
        if self.__query_set:  # not empty
            wid = random.choice(list(self.addr_table.keys()))
            self.sock.sendto(
                pickle.dumps(['GET', self.__query_set]),
                self.addr_table[wid][0])

    def get_avail_ids(self):
        """Discovers failure node and return the available list

        Removes the ROUND_CLEAN timed out records.
        Returns the list of the nodes within ROUND_FAIL bounds, and a recommended
            gossip message number
        Update self.sample based on current # of avail nodes
        Set sigterm
        """
        ret = []
        local = self.mlist
        cur = self.hb_num
        # Note for python3, local.keys() returns a view which is still dynamic
        # and doesn't allow modifying during iteration. The correct way is to
        # convert keys to a list and iterate
        for cid in list(local.keys()):
            # TODO add the voluntary leave nodes here, set its hb timestamp to inf
            if cur - local[cid][1] > ROUND_CLEAN:
                del local[cid]
                self.sigterm[2].remove(cid)
                logger.warning("Failed node removed:" + str(cid))
            # if this is a new node to be considered failed
            elif cid not in self.sigterm[2]:
                if cur - local[cid][1] > ROUND_FAIL:
                    # The daemon is responsible to reset this as False upon handling
                    # If a node is considered failed, remove from addr_table but
                    # keep it in the mlist until timeout ROUND_CLEAN
                    self.sigterm[1] = True
                    self.sigterm[2].append(cid)
                    del self.addr_table[cid]
                    logger.error("Failed node detected:" + str(cid))
                elif cid != self.wid:
                    ret.append(cid)
        num = len(ret)
        # calculate the gossip sample size based on current avail nodes
        sample = min(math.floor(math.sqrt(num) + 1), num)
        # sample = min(1, num)
        return ret, sample

    def merge(self, other, addr):
        """Merges with an incoming membership list

        merge happens after gossip_out in a round
        Args:
            other: the merged membership list
            addr: the address the merged list coming from
        """
        local = self.mlist
        for cid, timestamp in other:
            if cid not in local:  # not in local mlist
                if cid in self.sigterm[2]:
                    # TODO inform the other node that it has failed
                    pass
                else:  # New node! Query the source node for port info
                    if cid not in self.__query_set:
                        self.__query_set.add(cid)
                        self.sock.sendto(
                            pickle.dumps(['GET', self.__query_set]), addr)
            elif cid == self.wid:
                assert local[cid][1] >= timestamp, 'local timestamp smaller than remote!'
            else:
                local_entry = local[cid]
                if timestamp > local_entry[0]:
                    local_entry[0] = timestamp
                    local_entry[1] = self.hb_num
