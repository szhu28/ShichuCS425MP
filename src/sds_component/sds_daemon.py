"""
Daemon running on each node.

Including a failure detector thread, a list of task threads and a sdfs object
for disk I/O.
"""

import sys
import socket
import failure_detector
import threading
import sds_util
import signal
import argparse
import time
import logging
import pickle
import subprocess
import os

logger = sds_util.colored_log()

SRC = sds_util.SRC
# Same configuration across workers
NUM_WORKERS = sds_util.NUM_WORKERS
MAX_WORKERS = sds_util.MAX_WORKERS


class SDSDaemon:
    """Main daemon process running on every worker

    Attributes:
        addr_table: address table saving the 2 ports of each known worker. Type is
            {cid:(udp_hb_addr, tcp_addr)}
    """

    def __init__(self, standby=False, debug=True):
        """Initialize the parameters"""

        self.mem_list = None
        self.addr_table = None
        self.wid = None
        self.master_id = 0
        self.standby = standby  # existence of second master
        self.standby_id = -1
        self.log_enable = False

        self.host = socket.gethostname()
        self.fdtor = None  # failure_detector.FailureDetector()
        self.udp_hb = None
        self.tcp = None

        self.sigterm = [False]
        self.thread_pool = []
        self.debug_mode = debug

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def stop(self):
        """Stop the FD"""
        self.sigterm[0] = True

    # This function is only for demo purpose
    def sds_exec(self, cmd, block=True):
        """Run a bash command in batch for all VMs

        User {SRC} indicating the source code folder and {VM_ROOT} for the VM
        root folder and {WORKER_ID} for the id

        Arguments: cmd {str} -- bash command to execute, will be intepreted by
        shell

        Keyword Arguments: block {bool} -- blocking call or not default{True}
        """
        i = self.wid
        single_cmd = cmd.format(
            SRC=SRC, VM_ROOT=sds_util.get_config(i)['VM_ROOT'], WORKER_ID=i)
        logger.info("executing " + single_cmd)
        if block:
            proc = subprocess.run([single_cmd], shell=True, stdout=subprocess.PIPE)
            return proc.stdout
        else:
            subprocess.Popen([single_cmd], shell=True)

    def setup_ports(self):
        """Create the 2 ports and initialize the FD

        The self.wid needs to be set before calling, the function will overwrite
        the addr_table

        Ports: 1. UDP server for heartbeat 2. TCP server (for everything,
            data/instruction transmission, etc)
        """
        self.udp_hb = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_hb.bind((self.host, 0))
        self.tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp.bind((self.host, 0))
        self.tcp.listen(MAX_WORKERS)

        self.addr_table = {
            self.wid: [self.udp_hb.getsockname(),
                       self.tcp.getsockname()]
        }
        return self.addr_table.copy()

    def start_fdtor(self, addr_table=None):
        """Set fdtor field and Start failure detector

        The addr_table must be specific at this point, either previously
        assigned or passed in as argument
        """
        if self.addr_table is None:
            assert addr_table is not None
            self.addr_table = addr_table
        elif addr_table is not None:
            self.addr_table.update(addr_table)

        self.fdtor = failure_detector.FailureDetector(
            self.udp_hb,
            wid=self.wid,
            sigterm=self.sigterm,
            addr_table=self.addr_table)
        pthread = threading.Thread(target=self.fdtor.start_service)
        pthread.start()
        self.thread_pool.append(pthread)


if __name__ == "__main__":

    DAEMON = sds_util.remote_spawn_callee(args=sds_util.parse_manager())
    # with sds_util.remote_spawn_callee(args=sds_util.parse_manager()) as DAEMON:
    # while DAEMON.sigterm[0] is False:
    # This ensure master has enough time to call daemon.start
    # time.sleep(2)
    # for thread in DAEMON.thread_pool:
    # logger.info("Waiting for thread: " + str(thread))
    # thread.join()
    logger.info("Main thread exited of node " + str(DAEMON.wid))
